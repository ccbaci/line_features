#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/line_descriptor.hpp>

#define MASK_RATIO 0.95
#define MAX_DISTANCE 80
#define IMAGE_SIZE 512.0
#define SIMILARITY_THRESHOLD 10
#define INTER_PROPS_WEIGHT 0.6
#define INTRA_PROPS_WEIGHT 0.4

#define YELLOW cv::Scalar(0, 255, 255)
#define WHITE cv::Scalar(255, 255, 255)
#define RED cv::Scalar(0, 0, 255)

#define VISUALIZE

// used to determine if a point is inside the ROI (a circle)
bool isInsideMask(const cv::Point2d& point)
{
    return ((point.x - IMAGE_SIZE * 0.5) * (point.x - IMAGE_SIZE * 0.5) +
            (point.y - IMAGE_SIZE * 0.5) * (point.y - IMAGE_SIZE * 0.5) < IMAGE_SIZE * 0.5 * IMAGE_SIZE * 0.5 * MASK_RATIO * MASK_RATIO);
}

// used to collect good matches
class LineMatch
{
public:
    cv::Point2d centerPoint_1, centerPoint_2;
    cv::Point2d startPoint_1, startPoint_2;
    cv::Point2d endPoint_1, endPoint_2;
    double length_1, length_2;
    double angle_1, angle_2;
    double translation_x, translation_y, translation_angle;

    double score;

    LineMatch() {
        score = -INT_MAX;
    }

    bool isClose(const LineMatch &other) const {
        return (sqrt(pow(other.translation_x - translation_x, 2) + pow(other.translation_y - translation_y, 2)) < SIMILARITY_THRESHOLD);
    }
    bool operator< (const LineMatch &other) const {
        return (score < other.score);
    }
};

int main(int argc, char *argv[])
{
    if(argc < 2) {
        std::cout << "please enter an input argument \n  possible options = {c, b, a} \n  c: wood dataset 1, b: wood dataset 2, a: marker dataset" << std::endl;
        return -1;
    }

    std::string arg = argv[1];
    std::string prev_path = "sample_" + arg + "_1.png";
    std::string next_path = "sample_" + arg + "_2.png";

    // read images
    cv::Mat prev = cv::imread(prev_path);
    cv::Mat next = cv::imread(next_path);

    // for cycle count
    cv::TickMeter meter; meter.start();

    // resize to power of 2 for faster pyramid computation later on
    double inputWidth = prev.cols;
    double inputHeight = prev.rows;
    cv::resize(prev, prev, cv::Size(IMAGE_SIZE, IMAGE_SIZE), 0, 0, cv::INTER_CUBIC);
    cv::resize(next, next, cv::Size(IMAGE_SIZE, IMAGE_SIZE), 0, 0, cv::INTER_CUBIC);

    // prepare grayscale images
    cv::Mat prev_gray, next_gray;
    cv::cvtColor(prev, prev_gray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(next, next_gray, cv::COLOR_BGR2GRAY);

    // define the mask (region of interest) being the lens circle
    // we also define our ROI with an offset (defined by MASK_RATIO), so we discard lines on the edge
    cv::RotatedRect rect;
    rect.center = cv::Point2d(prev_gray.cols * 0.5, prev_gray.rows * 0.5);
    rect.size = cv::Size2d(prev_gray.cols * MASK_RATIO, prev_gray.rows * MASK_RATIO);

    // initialize detector, descriptor and matcher with default params
    auto lsd = cv::line_descriptor::LSDDetector::createLSDDetector();
    auto bd = cv::line_descriptor::BinaryDescriptor::createBinaryDescriptor();
    auto bdm = cv::line_descriptor::BinaryDescriptorMatcher::createBinaryDescriptorMatcher();
    std::vector<cv::line_descriptor::KeyLine> keylines_prev, keylines_next;
    cv::Mat desc_prev, desc_next;
    std::vector<cv::DMatch> matches;

    // detect lines on 2 pyramid levels
    lsd->detect(prev, keylines_prev, 2, 2);
    lsd->detect(next, keylines_next, 2, 2);

#ifdef VISUALIZE
    meter.stop();

    // prepare output (drawing) images
    cv::Mat prev_out = prev.clone();
    cv::Mat next_out = next.clone();
    cv::Mat image_matches(IMAGE_SIZE, IMAGE_SIZE * 2, CV_8UC3);
    cv::hconcat(prev, next, image_matches);
    cv::Mat image_bestMatch = image_matches.clone();

    // draw detected lines
    // filter out lines on the edge, just for better visualization
    for (const auto & line : keylines_prev) {
        if(isInsideMask(line.pt)) {
            cv::line(prev_out, line.getStartPoint(), line.getEndPoint(), YELLOW, 1);
            cv::ellipse(prev_out, rect, RED);
        }
    }
    for (const auto & line : keylines_next) {
        if(isInsideMask(line.pt)) {
            cv::line(next_out, line.getStartPoint(), line.getEndPoint(), YELLOW, 1);
            cv::ellipse(next_out, rect, RED);
        }
    }

    cv::imshow("lines_prev", prev_out);
    cv::waitKey(100);
    cv::imshow("lines_next", next_out);
    cv::waitKey(100);

    meter.start();
#endif

    // use a binary descriptor and compute the descriptions for provided list of key lines
    // we didnot filter the actual key lines vector, since the descriptor algorithm requires full detection information
    bd->setNumOfOctaves(2);
    bd->setReductionRatio(2);
    bd->compute(prev_gray, keylines_prev, desc_prev);
    bd->compute(next_gray, keylines_next, desc_next);

    // find the matches between descriptors
    bdm->match(desc_prev, desc_next, matches);

    // we need to filter the list of matches since it is noisy and also needs to be masked
    cv::RNG rng;
    std::vector<LineMatch> filtered_matches;
    for(const auto &match : matches) {
        // get center points of matching key lines
        const auto &line_prev = keylines_prev[match.queryIdx];
        const auto &line_next = keylines_next[match.trainIdx];

        // this is a good match if both center points are in ROI and L2 score is lower than our distance threshold
        if(isInsideMask(line_prev.pt) && isInsideMask(line_next.pt) && match.distance < MAX_DISTANCE)
        {
            // collect the properties of good matches
            LineMatch lineMatch;
            lineMatch.centerPoint_1 = line_prev.pt;
            lineMatch.centerPoint_2 = line_next.pt;
            lineMatch.length_1 = line_prev.lineLength;
            lineMatch.length_2 = line_next.lineLength;

            // angles (in degrees), translation and a "match score"
            lineMatch.startPoint_1 = line_prev.getStartPoint();
            lineMatch.startPoint_2 = line_next.getStartPoint();
            lineMatch.endPoint_1 = line_prev.getEndPoint();
            lineMatch.endPoint_2 = line_next.getEndPoint();

            // angles (arctan slope) of these lines should be calculated in input dimensions
            lineMatch.angle_1 = (180 / CV_PI) * atan(- ((inputHeight / IMAGE_SIZE) * (line_prev.endPointY - line_prev.startPointY)) / ((inputWidth / IMAGE_SIZE) * (line_prev.endPointX - line_prev.startPointX)));
            lineMatch.angle_2 = (180 / CV_PI) * atan(- ((inputHeight / IMAGE_SIZE) * (line_next.endPointY - line_next.startPointY)) / ((inputWidth / IMAGE_SIZE) * (line_next.endPointX - line_next.startPointX)));
            if(lineMatch.angle_1 < 0) lineMatch.angle_1 = 180 + lineMatch.angle_1;
            if(lineMatch.angle_2 < 0) lineMatch.angle_2 = 180 + lineMatch.angle_2;
            lineMatch.translation_angle = (180 / CV_PI) * atan(- ((inputHeight / IMAGE_SIZE) * (lineMatch.centerPoint_2.y - lineMatch.centerPoint_1.y)) / ((inputWidth / IMAGE_SIZE) * (lineMatch.centerPoint_2.x - lineMatch.centerPoint_1.x)));

            // translation y is inverted for convienience
            lineMatch.translation_x = lineMatch.centerPoint_2.x - lineMatch.centerPoint_1.x;
            lineMatch.translation_y = -(lineMatch.centerPoint_2.y - lineMatch.centerPoint_1.y);

            // assumption : zoom levels are identical, so matched line lengths should be similar
            // this should influence score of our current match
            // we factorize the initial match score with INTRA_PROPS_WEIGHT
            double lengthDiff = std::abs(lineMatch.length_1 - lineMatch.length_2);
            lineMatch.score = - (match.distance + lengthDiff) * INTRA_PROPS_WEIGHT;

            filtered_matches.push_back(lineMatch);

#ifdef VISUALIZE
            meter.stop();
            // visualize the points and draw the vector between them
            // use different colors for different matches
            cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

            // use a strong pen width if match score is higher (i.e. L2 norm is smaller)
            int penWidth; (match.distance < MAX_DISTANCE * 0.5) ? penWidth = 6 : penWidth = 1;
            cv::circle(image_matches, lineMatch.centerPoint_1, 4, color, penWidth);
            cv::circle(image_matches, cv::Point2d(lineMatch.centerPoint_2.x + IMAGE_SIZE, lineMatch.centerPoint_2.y), 4, color, penWidth);
            cv::line(image_matches, lineMatch.centerPoint_1, cv::Point2d(lineMatch.centerPoint_2.x + IMAGE_SIZE, lineMatch.centerPoint_2.y), color, penWidth);

            cv::imshow("matches", image_matches);
            cv::waitKey(1);
            meter.start();
#endif

        }
    }

    // increase the score of the good match for each similar vector found (factor INTER_PROPS_WEIGHT)
    for(uint i = 0; i < filtered_matches.size(); ++i) {
        for(uint j = 0; j < filtered_matches.size(); ++j) {
            if(i != j) {
                auto &m1 = filtered_matches[i];
                auto &m2 = filtered_matches[j];
                if(m1.isClose(m2)) m1.score += INTER_PROPS_WEIGHT;
            }
        }
    }

    // find the match vector having the best score
    std::sort(filtered_matches.begin(), filtered_matches.end());
    const LineMatch &bestMatch = filtered_matches.back();

    // provide the results
    // translation should be in input dimensions
    double tx_result = bestMatch.translation_x * (inputWidth / IMAGE_SIZE);
    double ty_result = bestMatch.translation_y * (inputHeight / IMAGE_SIZE);
    double angle_result = bestMatch.angle_2 - bestMatch.angle_1;
    std::cout << "(tx, ty, theta) = (" << tx_result << ", " << ty_result << ", " << angle_result << ")" << std::endl;

    // get cycle count
    meter.stop();
    std::cout << "time elapsed (ms): " << meter.getTimeMilli() << std::endl;

#ifdef VISUALIZE
    std::string text = "(tx, ty, theta) = " +
            std::to_string(tx_result) + ", " +
            std::to_string(ty_result) + ", " +
            std::to_string(bestMatch.angle_2) + " - " + std::to_string(bestMatch.angle_1) + " = " + std::to_string(angle_result) + " degrees (CCW +)";

    cv::line(image_bestMatch, bestMatch.centerPoint_1, bestMatch.centerPoint_2, YELLOW, 1);
    cv::circle(image_bestMatch, bestMatch.centerPoint_2, 2, YELLOW, 1);
    cv::circle(image_bestMatch, bestMatch.endPoint_1, 2, RED, 2);
    cv::circle(image_bestMatch, bestMatch.endPoint_2, 2, RED, 2);
    cv::line(image_bestMatch, bestMatch.startPoint_1, bestMatch.endPoint_1, RED, 2);
    cv::line(image_bestMatch, bestMatch.startPoint_2, bestMatch.endPoint_2, RED, 2);
    cv::line(image_bestMatch, cv::Point2d(bestMatch.startPoint_2.x + IMAGE_SIZE, bestMatch.startPoint_2.y), cv::Point2d(bestMatch.endPoint_2.x + IMAGE_SIZE, bestMatch.endPoint_2.y), RED, 2);
    cv::line(image_bestMatch, bestMatch.centerPoint_2, cv::Point2d(bestMatch.centerPoint_2.x + IMAGE_SIZE, bestMatch.centerPoint_2.y), YELLOW, 1);

    cv::putText(image_bestMatch, text, cv::Point(50, 50), cv::FONT_HERSHEY_COMPLEX, 0.5, WHITE);
    cv::imshow("best match", image_bestMatch);
    cv::waitKey();
#endif

    return 0;
}

